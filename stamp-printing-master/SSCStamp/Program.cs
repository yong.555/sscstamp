﻿using Microsoft.Win32;
using System;
using System.Threading;
using System.Windows.Forms;

namespace SSCStamp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            using (Mutex mutex = new Mutex(false, "Global\\" + Constants.APPLICATION_FULL_NAME))
            {
                if (!mutex.WaitOne(TimeSpan.Zero, false))
                {
                    Application.Exit();
                }
                else
                {
                    RegisterInStartup();

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    MainForm form = new MainForm();

                    Application.Run(form);

                    form.Hide();
                }
            }
        }

        private static void RegisterInStartup()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            registryKey.SetValue(Constants.APPLICATION_NAME, Application.ExecutablePath);
        }
    }
}