﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SSCStamp
{
    class AutoStart
    {
        private const string _autoStartKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";

        public bool AutoStartup
        {
            get
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(_autoStartKey, false))
                {
                    return key.GetValue(Constants.APPLICATION_NAME, null) != null;
                }
            }
            set
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(_autoStartKey, true))
                {
                    if (value)
                    {
                        key.SetValue(Constants.APPLICATION_NAME, $"\"{Process.GetCurrentProcess().MainModule.FileName}\"");
                    }
                    else
                    {
                        key.DeleteValue(Constants.APPLICATION_NAME, false);
                    }
                }
            }
        }
    }
}
