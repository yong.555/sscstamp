﻿using System;
using System.Configuration;

namespace SSCStamp
{
    public static class Utilities
    {
        public static T GetAppConfig<T>(this string value)
        {
            try
            {
                return ConfigurationManager.AppSettings[value].ConvertDataType<T>();
            }
            catch
            {
                return default(T);
            }
        }

        public static T ConvertDataType<T>(this object value)
        {
            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch
            {
                return default;
            }
        }
    }
}