﻿namespace SSCStamp
{
    internal class Constants
    {
        public const string APPLICATION_FULL_NAME = "SSC Stamp";
        public const string APPLICATION_NAME = "SSC_STAMP";
        public const string FOLDER_TO_WATCH = @"C:\SSC_Print";

        public const string FOLDER_PRINTED = "printeddir";
        public const string TIME_CLEAN_FOLDER_PRINTED = "timedelete";
        public const string TIME_START_DELETE_FILE = "timetorundeletefile";
    }
}