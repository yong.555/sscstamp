﻿using Microsoft.Win32;
using SSCStamp.Internals;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SSCStamp
{
    public partial class MainForm : Form
    {
        private FileSystemWatcher _watcher;
        private string _path;
        private string _printedPath;
        private bool _watching = false;
        private ConcurrentQueue<string> _queue = new ConcurrentQueue<string>();
        private Thread _queueThread;
        //private Thread _cleanThread;

        public MainForm()
        {
            InitializeComponent();

            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.Hide();

            this.lblVersion.Text = Application.ProductVersion;

            _watcher = new FileSystemWatcher();
            _path = Constants.FOLDER_TO_WATCH;
            _printedPath = Path.Combine(_path, "printed");

            InitWatch();
            InitTrayMenuContext();
        }

        private void InitTrayMenuContext()
        {
            this.notifyIconSSC.ContextMenuStrip = new ContextMenuStrip();
            this.notifyIconSSC.ContextMenuStrip.Items.Add("Thoát", null, this.btnExit_Click);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            StartListenQueue();
            StartWatch();
        }

        /// <summary>
        /// Handle resize, process hide application to tray
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }

        /// <summary>
        /// Prevent form closed and hide it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        /// <summary>
        /// When form is closed by other reason, dispose folder watcher and stop dequeue thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _watcher.EnableRaisingEvents = false;
            _watcher.Dispose();

            StopListenQueue();
        }

        /// <summary>
        /// Double click icon in tray, show form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIconSSC_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        #region Queue print

        /// <summary>
        /// Start listening queue
        /// </summary>
        private void StartListenQueue()
        {
            _watching = true;
            _queueThread = new Thread(new ThreadStart(this.ListenQueueThreadTask));
            _queueThread = new Thread(new ThreadStart(this.StartDeleteFilesBaseOnDate));

            _queueThread.IsBackground = true;
            _queueThread.Start();
        }

        /// <summary>
        /// Stop listening queue
        /// </summary>
        private void StopListenQueue()
        {
            _watching = false;
            _queueThread.Interrupt();
            _queueThread.Join();
        }

        /// <summary>
        /// Listen and dequeue continuously until _watching is false
        /// </summary>
        private void ListenQueueThreadTask()
        {
            try
            {
                while (_watching)
                {
                    string filepath;
                    while (_queue.TryDequeue(out filepath))
                    {
                        if (!File.Exists(filepath))
                        {
                            continue;
                        }

                        FileInfo fileInfo = new FileInfo(filepath);

                        if (fileInfo.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                        {
                            DoPrintFile(fileInfo.Name, filepath);
                            continue;
                        }

                        //if (fileInfo.Extension.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //    ExtractZip(filepath);
                        //    continue;
                        //}
                    }
                    Thread.Sleep(1000);
                }
            }
            catch (ThreadInterruptedException)
            {
                // Do nothing
            }
            catch (ThreadAbortException)
            {
                // Do nothing
            }
        }

        #endregion Queue print

        #region

        /// <summary>
        /// Delete files
        /// </summary>
        private void StartDeleteFilesBaseOnDate()
        {
            try
            {
                while (_watching)
                {
                    #region [Params]

                    var dirName = Utilities.GetAppConfig<string>(Constants.FOLDER_PRINTED);
                    var timmer = Utilities.GetAppConfig<int>(Constants.TIME_CLEAN_FOLDER_PRINTED);
                    var timeToRunJob = Utilities.GetAppConfig<int>(Constants.TIME_START_DELETE_FILE);

                    #endregion [Params]

                    #region [Functions]

                    if (!Directory.Exists(dirName)) return;

                    string[] files = Directory.GetFiles(dirName);
                    foreach (string file in files)
                    {
                        var fi = new FileInfo(file);
                        if (fi.LastWriteTime.Date < DateTime.Now.AddDays(timmer).Date)
                            fi.Delete();
                    }

                    #endregion [Functions]

                    Thread.Sleep(timeToRunJob);
                }
            }
            catch (ThreadInterruptedException)
            {
                // Do nothing
            }
        }

        #endregion

        #region Watch folder

        /// <summary>
        /// Initialize before watch
        /// </summary>
        /// <param name="path"></param>
        private void InitWatch()
        {
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }

            if (!Directory.Exists(_printedPath))
            {
                Directory.CreateDirectory(_printedPath);
            }
        }

        /// <summary>
        /// Start watching folder to print
        /// </summary>
        /// <param name="path"></param>
        private void StartWatch()
        {
            _watcher.Filter = "*.*";
            _watcher.Path = _path + "\\";
            _watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                     | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _watcher.Created += new FileSystemEventHandler(_watcher_Changed);
            _watcher.EnableRaisingEvents = true;
        }

        private void _watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                _queue.Enqueue(e.FullPath);
            }
        }

        #endregion

        #region Logic action

        /// <summary>
        /// Extract zip file
        /// </summary>
        /// <param name="filepath"></param>
        private void ExtractZip(string filepath)
        {
            using (Unzip unzip = new Unzip(filepath))
            {
                unzip.ExtractToDirectory(_path);
            }
            File.Delete(filepath);
        }

        /// <summary>
        /// Print file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filepath"></param>
        private void DoPrintFile(string filename, string filepath)
        {
            if (!File.Exists(filepath))
            {
                return;
            }

            string printer = DetectPrinterForFile(filename);
            if (printer.Equals(string.Empty))
            {
                return;
            }

            try
            {
                Thread.Sleep(500);
                SumatraPrint(filepath, printer);
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                string filePrintedPath = Path.Combine(_printedPath, filename);
                if (File.Exists(filePrintedPath))
                {
                    File.Delete(filePrintedPath);
                }

                try
                {
                    File.Move(filepath, filePrintedPath);
                }
                catch { }
            }
        }

        /// <summary>
        /// Get printer name for file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private string DetectPrinterForFile(string file)
        {
            //return "Godex GE330";
            if (file.StartsWith("100x150"))
            {
                return "100x150";
            }
            else if (file.StartsWith("100x80"))
            {
                return "100x80";
            }
            else if (file.StartsWith("50x20"))
            {
                return "50x20";
            }
            else if (file.StartsWith("50x50"))
            {
                return "50x50";
            }
            else if (file.StartsWith("A4"))
            {
                return "A4";
            }
            else
            {
                // Default printer
                //return new PrinterSettings().PrinterName;
                return string.Empty;
            }
        }

        /// <summary>
        /// Send print command to printer
        /// </summary>
        /// <param name="pdfFile"></param>
        /// <param name="printer"></param>
        private void SumatraPrint(string pdfFile, string printer)
        {
            var exePath = Registry.LocalMachine.OpenSubKey(
                @"SOFTWARE\Microsoft\Windows\CurrentVersion" +
                @"\App Paths\SumatraPDF.exe").GetValue("").ToString();

            var args = $"-print-to \"{printer}\" {pdfFile} -print-settings \"landscape\"";

            var process = Process.Start(exePath, args);
            if (process != null)
            {
                process.WaitForExit();
            }
            else
            {
                throw new Exception("Không thể chạy lệnh in bằng sumatra. Process start return null");
            }
        }

        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chắc chưa?", "Tắt?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                registryKey.DeleteValue(Constants.APPLICATION_NAME, false);

                Application.Exit();
            }
        }
    }
}